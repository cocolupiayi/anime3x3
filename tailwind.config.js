/* eslint-env node */
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,ts,tsx,js,jsx}"],
  theme: {
    extend: {
      animation: {
        "spin-slow": "spin 3s linear infinite",
        "slide-in": "slide-in 1s linear",
      },
      keyframes: {
        "slide-in": {
          "0%": {
            left: "100px",
            position: "absolute",
          },
          "100%": {
            left: "0px",
            position: "absolute",
          },
        },
      },
    },
  },
  plugins: [],
};
