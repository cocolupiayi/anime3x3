import React from "react";
import "./App.css";
import Search from "./components/Search/Search";
import { QueryClient, QueryClientProvider } from "react-query";
import { HTML5Backend } from "react-dnd-html5-backend";
import { DndProvider } from "react-dnd";
import Grid from "./components/Grid/Grid";
import { CookiesProvider } from "react-cookie";
import Tab from "./components/Tab/Tab";
import { isDesktop } from "react-device-detect";
import { TouchBackend } from "react-dnd-touch-backend";

const queryClient = new QueryClient();

function App() {
  return (
    <div className="App max-h-fit min-h-screen bg-stone-800">
      <QueryClientProvider client={queryClient}>
        <CookiesProvider>
          <DndProvider backend={!isDesktop ? TouchBackend : HTML5Backend}>
            <h1 className="pt-5 text-4xl font-bold text-white">
              Anime 3x3 Creator
            </h1>
            <Tab />
            <Grid />
          </DndProvider>
        </CookiesProvider>
      </QueryClientProvider>
    </div>
  );
}

export default App;
