export interface IJikanAnime {
  title: string;
  images: {
    jpg: {
      image_url: string;
    };
  };
}

export interface IJikanGetAnimeSearch {
  pagination: IJikanPagination;
  data: IJikanAnime[];
}

export interface IJikanPagination {
  last_visible_page: number;
  has_next_page: boolean;
  items: {
    count: number;
    total: number;
    per_page: number;
  };
}
