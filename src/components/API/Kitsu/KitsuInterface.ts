export interface KitsuInterface{
    data: KitsuAnimeInterface[];
    meta: {
        count: number;
    }
}

export interface KitsuAnimeInterface{
    id: string;
    type: string;
    attributes: {
        titles: {
            en: string;
        },
        posterImage: {
            original: string;
        }
    }
}

export interface KitsuCharactersInterface{
    id: string;
    type: string;
    attributes: {
        names: {
            en: string;
        },
        image: {
            original: string;
        }
    }
}
   