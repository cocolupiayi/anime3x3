export interface AnimeData {
    title: string;
    images: {
      jpg: {
        image_url: string;
      };
    };
  }
  
  export interface AnimeSearchInterface {
    pagination: PaginationInteface;
    data: AnimeData[];
  }
  
  export interface PaginationInteface {
    has_next_page: boolean;
    items: {
      count: number;
      total: number;
      per_page: number;
    };
  }