const defaultItems = [
  {
    title: "Mobile Suit Gundam SEED Destiny",
    url: "https://cdn.myanimelist.net/images/anime/1700/91080.jpg"
  },
  {
    title: "Clannad: After Story",
    url: "https://cdn.myanimelist.net/images/anime/1299/110774.jpg"
  },
  {
    title: "Aria",
    url: "https://cdn.myanimelist.net/images/anime/1431/132867.jpg"
  },
  {
    title: "Welcome to the NHK",
    url: "https://cdn.myanimelist.net/images/anime/3/52675.jpg"
  },
  {
    title: "Steins;Gate",
    url: "https://cdn.myanimelist.net/images/anime/5/73199.jpg"
  },
  {
    title: "Tsuki Ga Kirei",
    url: "https://cdn.myanimelist.net/images/anime/2/85592.jpg"
  },
  {
    title: "Higurashi no Naku Koro ni Kai",
    url: "https://cdn.myanimelist.net/images/anime/12/14114.jpg"
  },
  {
    title: "Darker than Black: Kuro no Keiyakusha",
    url: "https://cdn.myanimelist.net/images/anime/5/19570.jpg"
  },
  {
    title: "Sonny Boy",
    url: "https://cdn.myanimelist.net/images/anime/1509/117149.jpg"
  }
]

export default defaultItems;