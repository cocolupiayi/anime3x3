import React, { useEffect } from "react";
import { useCookies } from "react-cookie";
import { useDrag } from "react-dnd";
import { DragDropProps } from "../Search/SearchCard";

interface GridItemProps {
  item: DragDropProps;
  setItem: React.Dispatch<React.SetStateAction<DragDropProps | undefined>>;
  index: number;
}

const GridItem: React.FC<GridItemProps> = ({
  item,
  setItem,
  index
}: GridItemProps) => {
  const [cookies,setCookie] = useCookies([`item-${index}`]);
  const [{ isDragging }, drag, dragPreview] = useDrag(() => ({
    type: "GridBox",
    item: {
      url: item?.url,
      title: item?.title,
      index: index
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
    end: (currItem, monitor) => {
      setItem(undefined);
      setCookie(`item-${index}`,{});
    },
  }));

  return (
    <div className="max-h-full max-w-full overflow-hidden" ref={drag}>
      <img
        className="h-32 w-32 object-fill"
        key={item.title}
        src={item.url}
      ></img>
    </div>
  );
};

export default GridItem;
