import React, { useRef } from "react";
import OptionBar from "../OptionBar/OptionBar";
import GridCard from "./GridCard";

const Grid = () => {
  const screenshotRef = useRef<HTMLDivElement>(null);

  return (
    <>
      <div className="mt-8 flex justify-center">
        <div ref={screenshotRef} className="grid auto-cols-max grid-cols-3 justify-items-center">
          {
            [...Array(9).keys()].map((index)=><GridCard index={index} />)
          }
        </div>
      </div>
      <OptionBar ref={screenshotRef}/>
    </>  
  );
};

export default Grid;
