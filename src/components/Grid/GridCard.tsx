import React, { useEffect, useRef, useState } from "react";
import { useDrag, useDrop } from "react-dnd";
import { flushSync } from "react-dom";
import { DragDropProps } from "../Search/SearchCard";
import GridItem from "./GridItem";
import EventEmitter from "../Services/EventEmitter";
import defaultItems from "./GridInterface";
import { useCookies } from "react-cookie";

interface GridCardProps {
  index: number;
}

interface ItemProps extends DragDropProps {
  index: number;
}

const GridCard = ({ index }: GridCardProps) => {
  const [cookies, setCookie, removeCookie] = useCookies([`item-${index}`]);

  const [item, setItem] = useState<DragDropProps | undefined>(
    cookies[`item-${index}`] ?? defaultItems[index]
  );
  const itemRef = useRef<DragDropProps>();
  const [{ canDrop, isOver }, drop] = useDrop(() => ({
    accept: "GridBox",
    canDrop: (currItem) => index !== currItem.index,
    drop: (currItem: ItemProps) => {
      flushSync(() => {
        setItem(undefined);
      });
      flushSync(() => {
        setItem(currItem);
      });
      setCookie(`item-${index}`, currItem);
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
    }),
  }));

  const removeItem = () => {
    setItem(undefined);
    setCookie(`item-${index}`, {});
  };

  useEffect(() => {
    EventEmitter.on("removeItem", removeItem);
    return () => {
      EventEmitter.off("removeItem", removeItem);
    };
  }, []);

  useEffect(() => {
    itemRef.current = item;
  }, [item]);

  return (
    // <div className="flex justify-center items-center">
    <div
      ref={drop}
      className={`h-28 w-28 border-t-2 border-l-2 sm:h-32 sm:w-32
      ${index % 3 === 2 ? "border-r-2" : ""} 
      ${Math.floor(index / 3) === 2 ? "border-b-2" : ""}`}
    >
      {item && <GridItem item={item} setItem={setItem} index={index} />}
    </div>
    // </div>
  );
};

export default GridCard;
