import {EventEmitter as eventEmitter} from "eventemitter3";

const EventEmitter = new eventEmitter();

export default EventEmitter;