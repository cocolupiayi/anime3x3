import React, { useState } from "react";
import Search from "../Search/Search";
import SearchKitsu from "../Search/Kitsu/SearchKitsu";

interface SearchInfoInterface {
  title: string;
  placeHolder: string;
  queryUrl: string;
}

enum API {
  JIKAN = "Jikan",
  KITSU = "Kitsu",
}

export enum SEARCHTYPE {
  ANIME = "Anime",
  CHARACTER = "Character",
}

const newSearchInfo: Record<string, SearchInfoInterface> = {
  "Jikan-Anime": {
    title: "Search Anime Title using Jikan API",
    placeHolder: "E.g Clannad After Story",
    queryUrl: "https://api.jikan.moe/v4/anime?",
  },
  "Jikan-Character": {
    title: "Search Character Name using Jikan API",
    placeHolder: "E.g Vivy",
    queryUrl:
      "https://api.jikan.moe/v4/characters?&order_by=favorites&sort=desc",
  },
  "Kitsu-Anime": {
    title: "Search Anime Title using Kitsu API",
    placeHolder: "E.g Clannad After Story",
    queryUrl: "https://kitsu.io/api/edge/anime?",
  },
  "Kitsu-Character": {
    title: "Search Character Name using Kitsu API",
    placeHolder: "E.g Vivy",
    queryUrl: "https://kitsu.io/api/edge/characters?",
  },
};

const tabHeaders = ["Anime", "Characters"];
const apiHeaders = ["Jikan", "Kitsu"];
const titles = ["Enter Anime Name", "Enter Character Name"];

const Tab: React.FC = () => {
  const [currIndex, setCurrIndex] = useState<number>(0);

  const [selectedAPI, setSelectedAPI] = useState<API>(API.JIKAN);
  const [searchType, setSearchType] = useState<SEARCHTYPE>(SEARCHTYPE.ANIME);

  return (
    <>
      <ul className="mt-4 flex items-center justify-center gap-2 border-gray-200 dark:border-gray-700">
        {Object.values(SEARCHTYPE).map((val) => {
          return (
            <li>
              <a
                onClick={() => {
                  setSearchType(val);
                }}
                href="#"
                className={`${
                  searchType === val
                    ? "text-fuchsia-400"
                    : "text-gray-400 hover:text-gray-200 "
                }`}
              >
                {val}
              </a>
            </li>
          );
        })}
      </ul>
      <ul className="mt-4 flex items-center justify-center gap-2 border-gray-200 dark:border-gray-700">
        {Object.values(API).map((val) => {
          return (
            <li>
              <a
                onClick={() => {
                  setSelectedAPI(val);
                }}
                href="#"
                className={`${
                  selectedAPI === val
                    ? "text-fuchsia-400"
                    : "text-gray-400 hover:text-gray-200 "
                }`}
              >
                {val}
              </a>
            </li>
          );
        })}
      </ul>
      {selectedAPI === API.JIKAN && (
        <Search
          title={newSearchInfo[`${selectedAPI}-${searchType}`].title}
          placeHolder={
            newSearchInfo[`${selectedAPI}-${searchType}`].placeHolder
          }
          queryUrl={newSearchInfo[`${selectedAPI}-${searchType}`].queryUrl}
        />
      )}
      {selectedAPI === API.KITSU && (
        <SearchKitsu
          searchType={searchType}
          title={newSearchInfo[`${selectedAPI}-${searchType}`].title}
          placeHolder={
            newSearchInfo[`${selectedAPI}-${searchType}`].placeHolder
          }
          queryUrl={newSearchInfo[`${selectedAPI}-${searchType}`].queryUrl}
        />
      )}
    </>
  );
};

export default Tab;
