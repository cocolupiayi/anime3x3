import { saveAs } from "file-saver";
import { toBlob } from "html-to-image";
import React, { forwardRef, MutableRefObject, useRef } from "react";
import { MdDelete } from "react-icons/md";
import { TbFileExport } from "react-icons/tb";
import EventEmitter from "../Services/EventEmitter";


const OptionBar = forwardRef<HTMLElement>((props,ref) => {

  const removeAllItem = () => EventEmitter.emit("removeItem");

  const exportToImage = async() => {
    const currRef = ref as MutableRefObject<HTMLElement>;
    if(!currRef) return;
    const blob = await toBlob(currRef.current);
    if(!blob) return;
    saveAs(blob,"Anime3x3.png");
  }

  return (
    <div className="mt-8 flex justify-center align-middle">
      <div className="flex w-48 justify-around bg-stone-700 align-middle">
        <button onClick={removeAllItem} className="m-2 rounded-full p-2 hover:bg-stone-800/[.3] [&>*]:hover:fill-cyan-300">
          <MdDelete className="h-7 w-8 fill-white" />
        </button>
        <button onClick={exportToImage} className="m-2 rounded-full p-2 hover:bg-stone-800/[.3] [&>*]:hover:stroke-cyan-300">
          <TbFileExport className="h-7 w-8 stroke-white" />
        </button>
      </div>
    </div>
  );
});

export default OptionBar;
