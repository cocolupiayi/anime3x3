import axios from "axios";
import React, { useEffect, useState } from "react";
import { useQuery } from "react-query";
import {
  KitsuAnimeInterface,
  KitsuCharactersInterface,
  KitsuInterface,
} from "../../API/Kitsu/KitsuInterface";
import { AnimeData, AnimeSearchInterface } from "../../API/GenericAPIInterface";
import { GrFormNext, GrFormPrevious } from "react-icons/gr";
import { BiLoader } from "react-icons/bi";
import { IJikanAnime } from "../../API/Jikan/JikanInterface";
import SearchCard from "../SearchCard";
import { SEARCHTYPE } from "../../Tab/Tab";

interface SearchKitsuProps {
  searchType: SEARCHTYPE;
  title: string;
  placeHolder: string;
  queryUrl: string;
}

const SearchKitsu = ({
  title,
  placeHolder,
  queryUrl,
  searchType,
}: SearchKitsuProps) => {
  const [searchString, setSearchString] = useState<string>("");

  const [page, setPage] = useState<number>(0);

  const { isLoading, isError, data, error, refetch } = useQuery<
    AnimeSearchInterface | undefined
  >(["search", searchString, title], async () => {
    if (!searchString) return undefined;
    const url =
      searchType === SEARCHTYPE.ANIME
        ? `${queryUrl}fields[anime]=titles,posterImage&filter[text]=${searchString}&page[limit]=8&page[offset]=${
            page * 8
          }`
        : `${queryUrl}fields[characters]=names,image&filter[name]=${searchString}&page[limit]=8&page[offset]=${
            page * 8
          }`;
    const res = await axios.get(url);
    const result: AnimeSearchInterface = {
      pagination: {
        has_next_page: (res.data.meta.count as number) > (page + 1) * 8,
        items: {
          count: (page + 1) * 8,
          total: res.data.meta.count,
          per_page: 8,
        },
      },
      data:
        searchType === SEARCHTYPE.ANIME
          ? res.data.data.map((anime: KitsuAnimeInterface) => {
              return {
                title: anime.attributes.titles.en,
                images: {
                  jpg: {
                    image_url: anime.attributes.posterImage.original,
                  },
                },
              };
            })
          : res.data.data.map((anime: KitsuCharactersInterface) => {
              return {
                title: anime.attributes.names.en,
                images: {
                  jpg: {
                    image_url:
                      anime.attributes?.image?.original ?? "Missing Image Link",
                  },
                },
              };
            }),
    };
    console.log(result);
    return result;
  });

  useEffect(() => {
    refetch();
  }, [page]);

  useEffect(() => {
    setPage(0);
  }, [searchString, searchType, title]);

  return (
    <>
      <div className="flex items-center justify-center pt-5">
        <div className="mb-3 text-center xl:w-96">
          <label
            htmlFor="search"
            className="form-label mb-5 inline-block text-xl font-bold text-cyan-300"
          >
            {title}
          </label>
          <input
            placeholder={placeHolder}
            type="search"
            id="search"
            defaultValue={""}
            className="form-control m-0 block w-full bg-stone-700 text-white transition ease-linear placeholder:italic focus:border-cyan-300 focus:outline-none focus:ring-1 focus:ring-cyan-300"
            onChange={(e) => setSearchString(e.target.value)}
          />
        </div>
      </div>
      <div className="mt-5 flex items-center justify-center gap-x-5">
        {page !== 0 && (
          <GrFormPrevious
            onClick={() => {
              setPage((prevPage) => prevPage - 1);
            }}
            className="[&>polyline]:stroke-fuchsia-400"
            size="2rem"
          />
        )}
        <div className="flex items-center justify-center gap-x-2">
          {isLoading ? (
            <BiLoader
              className="animate-spin-slow text-fuchsia-400"
              size="2rem"
            />
          ) : (
            data?.data
              .filter((_: AnimeData, index: number) => index < 8)
              .map((anime: AnimeData, index: number) => (
                <SearchCard
                  key={`${anime.images.jpg.image_url}-${anime.title}-${index}`}
                  anime={anime}
                />
              ))
          )}
        </div>
        {data?.pagination.has_next_page && (
          <GrFormNext
            onClick={() => {
              console.log("click");
              setPage((prevPage) => prevPage + 1);
            }}
            className="[&>polyline]:stroke-fuchsia-400"
            size="2rem"
          />
        )}
      </div>
    </>
  );
};

export default SearchKitsu;
