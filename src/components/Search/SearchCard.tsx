import React, { useEffect } from "react";
import { useDrag } from "react-dnd";
import { AnimeData } from "../API/GenericAPIInterface";

interface SearchCardProps {
  anime: AnimeData;
}

export interface DragDropProps {
  url: string;
  title: string;
}

const SearchCard = ({ anime }: SearchCardProps) => {
  const [{ isDragging }, drag, dragPreview] = useDrag(() => ({
    type: "GridBox",
    item: {
      url: anime.images.jpg.image_url,
      title: anime.title,
    },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }));

  return (
    <>
      <div ref={drag}>
        <img
          // key={anime.title}
          className="h-32 w-32"
          src={anime.images.jpg.image_url}
          alt={anime.title}
        ></img>
      </div>
    </>
  );
};

export default SearchCard;
