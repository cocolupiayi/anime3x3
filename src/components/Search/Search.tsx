import axios from "axios";
import React, { useEffect, useState, useTransition } from "react";
import { useQuery } from "react-query";
import SearchCard from "./SearchCard";
import { BiLoader } from "react-icons/bi";
import { GrFormNext, GrFormPrevious } from "react-icons/gr";
import { IJikanAnime, IJikanGetAnimeSearch } from "../API/Jikan/JikanInterface";
import { AnimeData, AnimeSearchInterface } from "../API/GenericAPIInterface";

interface SearchProps {
  title: string;
  placeHolder: string;
  queryUrl: string;
}

const Search = ({ title, placeHolder, queryUrl }: SearchProps) => {
  const [searchString, setSearchString] = useState<string>("");

  const [page, setPage] = useState<number>(0);

  // const [isPending, startTransition] = useTransition();

  const { isLoading, isError, data, error, refetch } =
    useQuery<AnimeSearchInterface>(
      ["search", searchString, title],
      async () => {
        if (!searchString) return undefined;
        const res = await axios.get(
          `${queryUrl}&limit=8&page=${page + 1}&q=${searchString}&sfw`
        );
        return res.data;
      }
    );

  useEffect(() => {
    refetch();
  }, [page]);

  useEffect(() => {
    setPage(0);
  }, [searchString, title]);

  return (
    <>
      <div className="flex items-center justify-center pt-5">
        <div className="mb-3 text-center xl:w-96">
          <label
            htmlFor="search"
            className="form-label mb-5 inline-block text-xl font-bold text-cyan-300"
          >
            {title}
          </label>
          <input
            placeholder={placeHolder}
            type="search"
            id="search"
            defaultValue={""}
            className="form-control m-0 block w-full bg-stone-700 text-white transition ease-linear placeholder:italic focus:border-cyan-300 focus:outline-none focus:ring-1 focus:ring-cyan-300"
            onChange={(e) => setSearchString(e.target.value)}
          />
        </div>
      </div>
      <div className="mt-5 flex items-center justify-center gap-x-5">
        {page !== 0 && (
          <GrFormPrevious
            onClick={() => {
              setPage((prevPage) => prevPage - 1);
            }}
            className="[&>polyline]:stroke-fuchsia-400"
            size="2rem"
          />
        )}
        <div className="flex items-center justify-center gap-x-2">
          {isLoading ? (
            <BiLoader
              className="animate-spin-slow text-fuchsia-400"
              size="2rem"
            />
          ) : (
            data?.data
              .filter((_: AnimeData, index: number) => index < 8)
              .map((anime: AnimeData, index: number) => (
                <SearchCard
                  key={`${anime.images.jpg.image_url}-${anime.title}-${index}`}
                  anime={anime}
                />
              ))
          )}
        </div>
        {data?.pagination.has_next_page && (
          <GrFormNext
            onClick={() => {
              // console.log("click");
              setPage((prevPage) => prevPage + 1);
            }}
            className="[&>polyline]:stroke-fuchsia-400"
            size="2rem"
          />
        )}
      </div>
    </>
  );
};

export default Search;
